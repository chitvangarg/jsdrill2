function invert(obj){
    const newObj = {};

    if(obj != null){
        for(let key in obj){
            if(obj[key] != undefined)
                newObj[obj[key]] = key
        }
    }

    return newObj
}

module.exports = invert