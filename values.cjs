function values(obj){

    let values = [];

    if(obj != null){
        for(let key in obj){
            let value = obj[key]
            values.push(value)
        }
    }

    return values;
}

module.exports = values